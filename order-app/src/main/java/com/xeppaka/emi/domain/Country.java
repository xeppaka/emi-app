package com.xeppaka.emi.domain;

/**
 *
 */
public enum Country {
    CAN("Canada, Toronto"),
    CYP("Cyprus"),
    CZE("Czech Republic"),
    EST("Estonia"),
    FIN("Finland"),
    FRA("France"),
    GRE("Greece"),
    GER("Germany"),
    IRL("Ireland"),
    ISR("Israel"),
    ITA("Italy"),
    KOR("South Korea"),
    LAT("Latvia"),
    LIT("Lithuania"),
    POL("Poland"),
    POR("Portugal"),
    ROM("Romania"),
    RSA("South Africa"),
    SPA("Spain, Andalusia"),
    SWI("Switzerland"),
    TUN("Tunisia"),
    UAE("UAE"),
    UK("United Kingdom");

    private final String country;

    Country(String country) {
        this.country = country;
    }

    public String getCountry() {
        return country;
    }
}

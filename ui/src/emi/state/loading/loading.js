import update from 'react-addons-update';
import { LOAD_START, LOAD_FINISH_SUCCESS, LOAD_FINISH_FAIL } from './loadingactions';

const initialLoadingState = {
    isLoading: false
};

function loading(state = initialLoadingState, action) {
    switch (action.type) {
        case LOAD_START: {
            if (state.isLoading === false) {
                return update(state, {
                    isLoading: {$set: true}
                });
            }

            return state;
        }
        case LOAD_FINISH_SUCCESS: {
            if (state.isLoading === true) {
                return update(state, {
                    isLoading: {$set: false}
                });
            }

            return state;
        }
        case LOAD_FINISH_FAIL: {
            if (state.isLoading === true) {
                return update(state, {
                    isLoading: {$set: false}
                });
            }

            return state;
        }
        default:
            return state;
    }
}

export default loading;

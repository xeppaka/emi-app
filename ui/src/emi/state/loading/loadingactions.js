export const LOAD_START = 'LOAD_START';
export const LOAD_FINISH_SUCCESS = 'LOAD_FINISHED_SUCCESS';
export const LOAD_FINISH_FAIL = 'LOAD_FINISH_FAIL';

export function loadStart() {
    return { type: LOAD_START };
}

export function loadFinishSuccess() {
    return { type: LOAD_FINISH_SUCCESS }
}

export function loadFinishError(errorMessage = null) {
    return { type: LOAD_FINISH_FAIL, errorMessage: errorMessage }
}

import {setProducts, setFirstProductsVisible, loadQuantitiesFromLocalStorage} from '../products/productsactions';
import {setCategories} from "../categories/categoriesactions";
import {loadStart, loadFinishSuccess, loadFinishError} from "../loading/loadingactions";

export function loadWarehouse() {
    return function (dispatch) {
        dispatch(loadStart());
        return fetch('/api/warehouse').then(response => {
            if (response.status === 200) {
                return response.json()
            } else {
                return Promise.reject();
            }
        }).then(warehouseData => {
            dispatch(setCategories(warehouseData.categoryById));
            dispatch(setProducts(warehouseData.productById));
            dispatch(loadQuantitiesFromLocalStorage());

            dispatch(loadFinishSuccess());
            dispatch(setFirstProductsVisible());
            return Promise.resolve();
        }, reason => {
            loadFinishError();
        });
    };
}

export function bootstrapCustomer() {
    return function (dispatch) {
        return dispatch(loadWarehouse());
    }
}

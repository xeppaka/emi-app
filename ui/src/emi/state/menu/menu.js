import update from 'react-addons-update';
import { SELECT_MENU_NODE } from './menuactions';

const initialMenuState = {
    selectedNodeId: ''
};

function menu(state = initialMenuState, action) {
    switch (action.type) {
        case SELECT_MENU_NODE: {
            if (state.selectedNodeId !== action.selectedNodeId) {
                return update(state, {
                    selectedNodeId: {$set: action.selectedNodeId}
                });
            }

            return state;
        }
        default:
            return state;
    }
}

export default menu;

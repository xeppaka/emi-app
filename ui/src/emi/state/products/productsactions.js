import { productIdsSelector } from '../selectors/productsselector';
import { showImageModal } from '../modals/modalsactions';

export const SET_PRODUCTS = 'SET_PRODUCTS';
export const UPDATE_PRODUCTS = 'UPDATE_PRODUCTS';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const SET_PRODUCT_QUANTITY = 'SET_PRODUCT_QUANTITY';
export const PRODUCTS_RESET = 'PRODUCTS_RESET';
export const PRODUCT_SHOWN = 'PRODUCT_SHOWN';

export function setProducts(productById) {
    return { type: SET_PRODUCTS, productById: productById }
}

export function loadQuantitiesFromLocalStorage() {
    return function(dispatch, getState) {
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            if (key.startsWith('product.')) {
                let productId = key.substring(8);
                let value = Number(localStorage.getItem(key));

                dispatch(setProductQuantity(productId, value));
            }
        }
    }
}

export function updateProducts(products) {
    return { type: UPDATE_PRODUCTS, products: products }
}

export function removeProduct(productId) {
    return { type: REMOVE_PRODUCT, productId: productId }
}

export function setProductQuantity(productId, quantityValue) {
    return { type: SET_PRODUCT_QUANTITY, productId: productId, value: quantityValue }
}

export function productsReset() {
    return { type: PRODUCTS_RESET }
}

function setProductsShown(productIds, indexFrom) {
    return { type: PRODUCT_SHOWN, productIds: productIds, indexFrom: indexFrom }
}

export function productShown(productId) {
    return function (dispatch, getState) {
        let productIds = productIdsSelector(getState());
        let productIdsSource = productIds.mainProductIds;
        let productIndex = productIdsSource.indexOf(productId);

        if (productIndex < 0) {
            productIdsSource = productIds.posProductIds;
            productIndex = productIdsSource.indexOf(productId);
        }

        if (productIndex >= 0) {
            dispatch(setProductsShown(productIdsSource, productIndex));
        }
    };
}

export function setFirstProductsVisible() {
    return function (dispatch, getState) {
        let productIds = productIdsSelector(getState());
        let productIdsSource = productIds.mainProductIds;

        if (productIdsSource.length > 0) {
            dispatch(productShown(productIdsSource[0]));
        }
    };
}

export function showImageForProduct(productId) {
    return function (dispatch, getState) {
        let state = getState();
        let product = state.emiapp.warehouse.products.productById[productId];
        let title = product.name;
        let imageLink = product.image;

        dispatch(showImageModal(title, imageLink));
    };
}

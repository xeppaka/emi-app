import update from 'react-addons-update';
import {
    UPDATE_PRODUCTS, SET_PRODUCT_QUANTITY, PRODUCTS_RESET, REMOVE_PRODUCT, SET_PRODUCTS,
    PRODUCT_SHOWN
} from './productsactions';

const initialProductsState = {
    productById: {}
};

function initializeProducts(productById) {
    for (let key in productById) {
        if (!productById.hasOwnProperty(key))
            continue;

        let product = productById[key];
        product.quantity = 0;
        product.shown = false;
    }

    return productById;
}

function initializeProductsList(products) {
    for (let i = 0; i < products.length; i++) {
        let product = products[i];
        product.quantity = 0;
        product.shown = false;
    }

    return products;
}

function setLocalProductQuantity(productId, quantityValue) {
    if (quantityValue > 0) {
        localStorage.setItem("product." + productId, quantityValue);
    } else {
        localStorage.removeItem("product." + productId);
    }
}

function clearLocalProductQuantities() {
    let productKeysToRemove = [];

    for (let i = 0; i < localStorage.length; i++) {
        let key = localStorage.key(i);
        if (key.startsWith('product.')) {
            productKeysToRemove.push(key);
        }
    }

    for (let i = 0; i < productKeysToRemove.length; i++) {
        localStorage.removeItem(productKeysToRemove[i]);
    }
}

function products(state = initialProductsState, action) {
    switch (action.type) {
        case SET_PRODUCTS:
            return update(state, {
                productById: {$set: initializeProducts(action.productById)}
            });
        case UPDATE_PRODUCTS:
            let updatedProducts = initializeProductsList(action.products);
            let updatedState = state;

            for (let i = 0; i < updatedProducts.length; i++) {
                updatedState = update(updatedState, {
                    productById: {
                        [updatedProducts[i].productId]: {$set: updatedProducts[i]}
                    }
                });
            }

            return updatedState;
        case REMOVE_PRODUCT: {
            let productId = action.productId;
            let updatedState = update(state, {
                productById: {
                    [productId]: {$set: null}
                }
            });

            delete updatedState.productById[productId];
            return updatedState;
        }
        case SET_PRODUCT_QUANTITY: {
            if (!state.productById.hasOwnProperty(action.productId)) {
                return state;
            }

            setLocalProductQuantity(action.productId, action.value);

            return update(state, {
                productById: {
                    [action.productId]: {
                        quantity: {$set: action.value}
                    }
                }
            });
        }
        case PRODUCTS_RESET: {
            clearLocalProductQuantities();

            let productById = state.productById;
            let newProductById = {};

            for (let key in productById) {
                if (!productById.hasOwnProperty(key))
                    continue;

                let product = productById[key];

                if (product.quantity !== 0) {
                    newProductById[product.productId] = update(product, {
                        quantity: {$set: 0}
                    });
                } else {
                    newProductById[product.productId] = product;
                }
            }

            return update(state, {
                productById: {$set: newProductById}
            });
        }
        case PRODUCT_SHOWN: {
            let productIds = action.productIds;
            let indexFrom = action.indexFrom;
            let updatedState = state;

            let productsUpdated = 0;
            let i = indexFrom;
            while (productsUpdated < 17 && i < productIds.length) {
                let productId = productIds[i];
                let product = updatedState.productById[productId];

                if (product.features.indexOf('VISIBLE') >= 0) {
                    if (product.shown !== true) {
                        updatedState = update(updatedState, {
                            productById: {
                                [productId]: {
                                    shown: {$set: true}
                                }
                            }
                        });
                    }

                    productsUpdated += 1;
                }

                i += 1;
            }

            return updatedState;
        }
        default:
            return state;
    }
}

export default products;

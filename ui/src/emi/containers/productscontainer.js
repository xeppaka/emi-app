import { connect } from 'react-redux';
import ProductsTables from '../components/customer/productstable/productstables';
import { setProductQuantity, showImageForProduct } from '../state/products/productsactions';
import { selectMenuNode } from '../state/menu/menuactions';
import { productShown } from '../state/products/productsactions';
import { mainProductsSelector, posProductsWithLeftAmountSelector } from '../state/selectors/productsselector';

const mapStateToProps = (state) => {
    return {
        mainProducts: mainProductsSelector(state),
        posProducts: posProductsWithLeftAmountSelector(state),
        isLoading: state.emiapp.loading.isLoading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        setProductQuantity: (productId, value) => {
            dispatch(setProductQuantity(productId, value));
        },
        onShowProductImage: (productId) => {
            dispatch(showImageForProduct(productId));
        },
        scrolledToProduct: (productId, menuAnchor) => {
            dispatch(selectMenuNode(menuAnchor));
            dispatch(productShown(productId));
        }
    }
};

const ProductsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductsTables);

export default ProductsContainer;

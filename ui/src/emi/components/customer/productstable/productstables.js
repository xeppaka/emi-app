import React, {PropTypes} from 'react';
import MainProductsTable from './mainproductstable';
import PosProductsTable from './posproductstable';

class ProductsTables extends React.Component {
    constructor(props) {
        super(props);
        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        let product = this.refs['mainProductsContainer'].getVisibleProduct();
        if (product === null) {
            product = this.refs['posProductsContainer'].getVisibleProduct();
        }

        if (product !== null) {
            this.props.scrolledToProduct(product.product.productId, product.anchor.name);
        }
    }

    renderLoading() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <h5 className='col-sm-9'>Please wait while products data is loading...</h5>
                </div>
            </div>
        )
    }

    renderTables() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <MainProductsTable ref='mainProductsContainer'
                                       products={this.props.mainProducts}
                                       setProductQuantity={this.props.setProductQuantity}
                                       onShowProductImage={this.props.onShowProductImage}
                    />
                </div>
                <div className="row">
                    <PosProductsTable ref='posProductsContainer'
                                      products={this.props.posProducts}
                                      setProductQuantity={this.props.setProductQuantity}
                                      onShowProductImage={this.props.onShowProductImage}
                    />
                </div>
            </div>
        )
    }

    render() {
        if (this.props.isLoading) {
            return this.renderLoading();
        } else {
            return this.renderTables();
        }
    }
}

export default ProductsTables;